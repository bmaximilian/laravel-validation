/**
 * Created on 24.09.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { isFunction, isNumber, isString } from 'lodash';

export default class ValidationToken {
  constructor(
    fn,
    valuesRequired = false,
    minValues = 0,
    maxValues = null,
    customValidation = null,
  ) {
    this.function = (value = null, token = '', other = null) => {
      if (isFunction(fn)) {
        if (this.isValidToken(token)) {
          const match = /^(.*?):(.*?)$/.exec(token.trim());
          if (match.length === 3) {
            return fn(value, match[2], other);
          }
          return fn(value, token, other);
        }
        throw new Error(`The validation token ${token} is invalid`);
      } else {
        throw new Error('The validation token has no valid function');
      }
    };
    this.valuesRequired = valuesRequired;
    this.minValues = minValues;
    this.maxValues = maxValues;
    this.customValidation = customValidation;
  }

  isValidToken(token = '', strict = false) {
    if (isString(token)) {
      const match = /^(.*?):(.*?)$/.exec(token.trim());
      if (this.valuesRequired && match.length === 3 && match[2].length > 0) {
        const valueArray = match[2].toString().split(',');
        if (this.validMinLength(valueArray.length) && this.validMaxLength(valueArray.length)) {
          if (isFunction(this.customValidation)) {
            return this.customValidation(valueArray);
          }
          return true;
        }
        return false;
      } else if (!this.valuesRequired && match && strict) {
        return false;
      }
      if (isFunction(this.customValidation)) {
        return this.customValidation();
      }
      return true;
    }
    return false;
  }

  validMinLength(length = 0) {
    if (isNumber(length) && isNumber(this.minValues)) {
      return this.minValues === 0 || length >= this.minValues;
    }
    return false;
  }

  validMaxLength(length = 0) {
    if (isNumber(length) && isNumber(this.maxValues)) {
      return this.maxValues === null || length <= this.maxValues;
    }
    return false;
  }
}
