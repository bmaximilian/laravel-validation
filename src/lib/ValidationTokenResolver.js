/**
 * Created on 24.09.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { has, includes, indexOf, isArray, isNull, isObject, isString, isUndefined } from 'lodash';
import ValidationToken from './ValidationToken';

/**
 * Supported data types (string, integer, numeric(float), 'boolean')
 * have to be extracted before running through validation function to parse the value into the type.
 * If the token has no matched type the value will be treated as string
 * Nullable should be validated last
 */

export default class ValidationTokenResolver {
  tokens = {
    /**
     * The field under validation must be yes, on, 1, or true
     */
    accepted: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for accepted token */
      }
      return false;
    }),
    /**
     * The field under validation must be a value after a given date
     * Can match 'now', 'yesterday', 'tomorrow', an european date or an american date
     */
    after: new ValidationToken((value, token) => {
      if (value !== null && isString(token)) {
        /* TODO: Add function for after token */
      }
      return false;
    }, true, 1, 1),
    /**
     * The field under validation must be a value after or equal to the given date
     * Can match 'now', 'yesterday', 'tomorrow', an european date or an american date
     */
    after_or_equal: new ValidationToken((value, token) => {
      if (value !== null && isString(token)) {
        /* TODO: Add function for after_or_equal token */
      }
      return false;
    }, true, 1, 1),
    /**
     * The field under validation must be entirely alphabetic characters
     */
    alpha: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for alpha token */
      }
      return false;
    }),
    /**
     * The field under validation may have alpha-numeric characters,
     * as well as dashes and underscores.
     */
    alpha_dash: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for alpha_dash token */
      }
      return false;
    }),
    /**
     * The field under validation must be entirely alpha-numeric characters.
     */
    alpha_num: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for alpha_num token */
      }
      return false;
    }),
    array: new ValidationToken(value => isArray(value) || isObject(value)),
    /**
     * The field under validation must be a value preceding the given date
     * The dates will be passed into the PHP strtotime function.
     * Can match 'now', 'yesterday', 'tomorrow', an european date or an american date
     */
    before: new ValidationToken((value, token) => {
      if (value !== null && token !== null) {
        /* TODO: Add function for before token */
      }
      return false;
    }, true, 1, 1),
    /**
     * The field under validation must be a value preceding or equal to the given date.
     * The dates will be passed into the PHP strtotime function.
     * Can match 'now', 'yesterday', 'tomorrow', an european date or an american date
     */
    before_or_equal: new ValidationToken((value, token) => {
      if (value !== null && token !== null) {
        /* TODO: Add function for before_or_equal token */
      }
    }, true, 1, 1),
    /**
     * The field under validation must have a size between the given min and max
     * Strings, numerics, arrays, and files are evaluated in the same fashion as the size rule.
     */
    between: new ValidationToken((value, token) => {
      if (value !== null && isString(token)) {
        /* TODO: Add function for between token */
      }
      return false;
    }, true, 2, 2),
    /**
     * The field under validation must be able to be cast as a boolean.
     * Accepted input are true, false,  1, 0, "1", and "0".
     */
    boolean: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for boolean token */
      }
      return false;
    }),
    /**
     * The field under validation must have a matching field of foo_confirmation.
     * For example, if the field under validation is password
     * a matching password_confirmation field must be present in the input.
     */
    confirmed: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for confirmed token */
      }
      return false;
    }),
    /**
     * The field under validation must be a valid date according to the strtotime PHP function.
     */
    date: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for date token */
      }
      return false;
    }),
    /**
     * The field under validation must be equal to the given date.
     * The dates will be passed into the PHP  strtotime function.
     * Can match 'now', 'yesterday', 'tomorrow', an european date or an american date
     */
    date_equals: new ValidationToken((value, token) => {
      if (value !== null && isString(token)) {
        /* TODO: Add function for date_equals token */
      }
      return false;
    }, true, 1, 1),
    /**
     * The field under validation must match the given format.
     * You should use either date or  date_format when validating a field, not both.
     */
    date_format: new ValidationToken((value, token) => {
      if (value !== null && isString(token)) {
        /* TODO: Add function for date_format token */
      }
      return false;
    }, true, 1, 1),
    /**
     * The field under validation must have a different value than field.
     */
    different: new ValidationToken((value, token, object = null) => {
      if (value !== null && isString(token) && (isObject(object) || isNull(object))) {
        /* TODO: Add function for different token */
      }
      return false;
    }, true, 1, 1),
    /**
     * The field under validation must be numeric and must have an exact length of value.
     */
    digits: new ValidationToken((value, token) => {
      if (value !== null && isString(token)) {
        /* TODO: Add function for digits token */
      }
      return false;
    }, true, 1, 1),
    /**
     * The field under validation must have a length between the given min and max.
     */
    digits_between: new ValidationToken((value, token) => {
      if (value !== null && isString(token)) {
        /* TODO: Add function for digits_between token */
      }
      return false;
    }, true, 2, 2),
    /**
     * The file under validation must be an image
     * meeting the dimension constraints as specified by the rule's parameters
     */
    dimensions: new ValidationToken((value, token) => {
      if (value !== null && isString(token)) {
        /* TODO: Add function for dimensions token */
      }
      return false;
    }, true, 2, 2, (values = []) => {
      let valid = true;
      values.every((value) => {
        if (valid) {
          const valueArray = value.split('=');
          if (valueArray.length === 2) {
            valid = includes(['min_width', 'max_width', 'min_height', 'max_height', 'width', 'height', 'ratio'], valueArray[0]);
          } else {
            valid = false;
          }
          return valid;
        }
        return false;
      });
      return valid;
    }),
    /**
     * When working with arrays, the field under validation must not have any duplicate values.
     */
    distinct: new ValidationToken((value) => {
      if (value !== null && isArray(value)) {
        /* TODO: Add function for distinct token */
      }
      return false;
    }),
    /**
     * The field under validation must be formatted as an e-mail address.
     */
    email: new ValidationToken((value) => {
      if (isString(value)) {
        /* TODO: Add function for email token */
      }
      return false;
    }),
    /**
     * The field under validation must not be empty when it is present.
     */
    filled: new ValidationToken((value, token) => {
      if (value !== null && (isString(token) || isNull(token))) {
        /* TODO: Add function for filled token */
      }
      return false;
    }, true, 0, 1),
    /**
     * The file under validation must be an image (jpeg, png, bmp, gif, or svg)
     */
    image: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for image token */
      }
      return false;
    }),
    /**
     *  The field under validation must be included in the given list of values.
     */
    in: new ValidationToken((value, token) => {
      if (value !== null && isString(token)) {
        /* TODO: Add function for in token */
      }
      return false;
    }, true, 1),
    /**
     * The field under validation must exist in anotherfield's values.
     */
    in_array: new ValidationToken((value, token) => {
      if (value !== null && isString(token)) {
        /* TODO: Add function for in_array token */
      }
      return false;
    }, true, 1, 1),
    /**
     * The field under validation must be an integer.
     */
    integer: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for integer token */
      }
      return false;
    }),
    /**
     * The field under validation must be an IP address.
     */
    ip: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for ip token */
      }
      return false;
    }),
    /**
     * The field under validation must be an IPv4 address.
     */
    ipv4: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for ipv4 token */
      }
      return false;
    }),
    /**
     * The field under validation must be an IPv6 address.
     */
    ipv6: new ValidationToken((value) => {
      if (value !== null && isString(value) && value.length > 0) {
        /* TODO: Add function for ipv6 token */
      }
      return false;
    }),
    /**
     * The field under validation must be a valid JSON string.
     */
    json: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for json token */
      }
      return false;
    }),
    /**
     * The field under validation must be less than or equal to a maximum value.
     * Strings, numerics, arrays, and files are evaluated in the same fashion as the size rule.
     */
    max: new ValidationToken((value, token) => {
      if (value !== null && token !== null) {
        /* TODO: Add function for max token */
      }
      return false;
    }, true, 1, 1),
    /**
     * The field under validation must have a minimum value.
     * Strings, numerics, arrays, and files are evaluated in the same fashion as the size rule.
     */
    min: new ValidationToken((value, token) => {
      if (value !== null && token !== null) {
        /* TODO: Add function for min token */
      }
      return false;
    }, true, 1, 1),
    /**
     * The field under validation may be null.
     * This is particularly useful when validating primitive
     * such as strings and integers that can contain null values.
     */
    nullable: new ValidationToken(value => isNull(value) || isUndefined(value)),
    /**
     * The field under validation must not be included in the given list of values
     */
    not_in: new ValidationToken((value, token) => {
      if (value !== null && isString(token) && token.toString().trim().length > 0) {
        /* TODO: Add function for not_in token */
      }
      return false;
    }, true, 1),
    /**
     * The field under validation must be numeric.
     */
    numeric: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for numeric token */
      }
      return false;
    }),
    /**
     * The field under validation must be present in the input data but can be empty.
     */
    present: new ValidationToken(() => true),
    /**
     * The field under validation must match the given regular expression.
     */
    regex: new ValidationToken((value, token) => {
      if (value !== null && token !== null) {
        /* TODO: Add function for regex token */
      }
      return false;
    }, true, 1, 1),
    /**
     * A field is considered "empty" if one of the following conditions are true:
     *   The value is null.
     *   The value is an empty string.
     *   The value is an empty array or empty Countable object.
     *   The value is an uploaded file with no path.
     */
    required: new ValidationToken((value) => {
      if (value !== null) {
        /* TODO: Add function for required token */
      }
      return false;
    }),
    /**
     * The field under validation must be present
     * and not empty if the anotherfield field is equal to any value.
     */
    required_if: new ValidationToken((value, token) => {
      if (value !== null && token !== null && isString(token)) {
        /* TODO: Add function for required_if token */
      }
      return false;
    }, true, 2),
    /**
     * The field under validation must be present
     * and not empty unless the anotherfield field is equal to any value.
     */
    required_unless: new ValidationToken((value, token) => {
      if (value !== null && token !== null && isString(token)) {
        /* TODO: Add function for required_unless token */
      }
      return false;
    }, true, 2),
    /**
     * The field under validation must be present
     * and not empty only if any of the other specified fields are present.
     */
    required_with: new ValidationToken((value, token) => {
      if (value !== null && token !== null && isString(token)) {
        /* TODO: Add function for required_with token */
      }
      return false;
    }, true, 1),
    /**
     * The field under validation must be present
     * and not empty only if all of the other specified fields are present.
     */
    required_with_all: new ValidationToken((value, token) => {
      if (value !== null && token !== null && isString(token)) {
        /* TODO: Add function for required_with_all token */
      }
      return false;
    }, true, 1),
    /**
     * The field under validation must be present
     * and not empty only when any of the other specified fields are not present.
     */
    required_without: new ValidationToken((value, token) => {
      if (value !== null && token !== null && isString(token)) {
        /* TODO: Add function for required_without token */
      }
      return false;
    }, true, 1),
    /**
     * The field under validation must be present
     * and not empty only when all of the other specified fields are not present.
     */
    required_without_all: new ValidationToken((value, token) => {
      if (value !== null && token !== null && isString(token)) {
        /* TODO: Add function for required_without_all token */
      }
      return false;
    }, true, 1),
    /**
     * The given field must match the field (or value) under validation.
     */
    same: new ValidationToken((value, token) => {
      if (value !== null && token !== null) {
        /* TODO: Add function for same token */
      }
      return false;
    }, true, 1, 1),
    /**
     * The field under validation must have a size matching the given value.
     *   For string data, value corresponds to the number of characters.
     *   For numeric data, value corresponds to a given integer value.
     *   For an array, size corresponds to the count of the array.
     *   For files, size corresponds to the file size in kilobytes.
     */
    size: new ValidationToken((value, token) => {
      if (value !== null && token !== null) {
        /* TODO: Add function for size token */
      }
      return false;
    }, true, 1, 1),
    /**
     * The field under validation must be a string
     * If you would like to allow the field to also be null
     * you should assign the nullable rule to the field.
     */
    string: new ValidationToken(value => value !== null && isString(value)),
    /**
     * The field under validation must be a valid timezone identifier
     * according to the  timezone_identifiers_list PHP function.
     */
    timezone: new ValidationToken((value) => {
      if (value !== null && value.toString().trim().length > 0) {
        /* TODO: Add function for timezone token */
      }
      return false;
    }),
    /**
     * Checks if a given array has unique values
     */
    unique: new ValidationToken((value) => {
      if (value !== null && isArray(value)) {
        /* TODO: Add function for unique token */
      }
      return false;
    }),
    /**
     * The field under validation must be a valid URL.
     */
    url: new ValidationToken((value) => {
      if (value !== null && value.toString().trim().length > 0) {
        /* TODO: Add function for url token */
      }
      return false;
    }),
  };

  valid = false;

  constructor(token = '', value = null, other = null, messages = null, parse = true) {
    this.token = token;
    this.value = value;
    this.parse = parse;
    this.other = other;
    this.messages = messages;
    this.validationArray = token.split('|');

    this.indexes = {
      nullable: { index: indexOf(this.validationArray, 'nullable'), position: 'last' },
      string: { index: indexOf(this.validationArray, 'string'), position: 'first' },
      integer: { index: indexOf(this.validationArray, 'integer'), position: 'first' },
      numeric: { index: indexOf(this.validationArray, 'numeric'), position: 'first' },
      boolean: { index: indexOf(this.validationArray, 'boolean'), position: 'first' },
    };

    this.resolveToken();
  }

  resolveToken() {
    if (this.validationArray.length > 0) {
      const beforeAll = [];
      const afterAll = [];

      Object.keys(this.indexes).forEach((key) => {
        if (has(this.tokens, key) && this.indexes[key].index > -1) {
          switch (this.indexes[key].position) {
            case 'first':
              beforeAll.push(key);
              this.validationArray.splice(this.indexes[key].index, 1);
              break;
            case 'last':
              afterAll.push(key);
              this.validationArray.splice(this.indexes[key].index, 1);
              break;
            default:
              break;
          }
        }
      });

      this.checkValidity(beforeAll);
      this.checkValidity();
      this.checkValidity(afterAll, true);
    }
  }

  checkValidity(tokens = this.validationArray, override = false) {
    if (this.valid || override) {
      tokens.every((token) => {
        this.valid = this.tokens[token].function(this.value, this.token, this.other);
        return this.valid;
      });
    }
  }
}
