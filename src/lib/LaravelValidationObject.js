/**
 * Created on 21.07.17.
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */
class LaravelValidationObject {
  /**
   * Constructor of LaravelValidationObject
   * @param token
   * @param isRequired
   */
  constructor(token, isRequired = null) {
    this.token = token;
    this.type = '';
    this.rules = [];
    if (isRequired === null) {
      this.required = false;
    } else {
      this.required = JSON.parse(isRequired);
    }
    this.resolveToken();
  }

  /**
   * Function to resolve the laravel validation token
   * The token should be like this:
   * <data_type>|<rule:value>|<rule:value>..
   */
  resolveToken() {
    if (this.token) {
      const validationArray = this.token.split('|');
      if (validationArray.length > 0) {
        this.type = validationArray.splice(0, 1)[0];
        if (this.type.toLowerCase() !== 'integer'
          && this.type.toLowerCase() !== 'numeric'
          && this.type.toLowerCase() !== 'string') {
          validationArray.push(this.type);
        }

        for (let i = 0; i < validationArray.length; i += 1) {
          if (validationArray[i].toLowerCase() === 'required') {
            this.required = true;
          } else if ((this.type.toLowerCase() === 'integer'
              || this.type.toLowerCase() === 'numeric'
              || this.type.toLowerCase() === 'string') && this.type.length === 0) {
            this.type = validationArray[i];
          } else {
            const ruleArray = validationArray[i].split(':');

            if (ruleArray && ruleArray.length > 1) {
              this.rules.push({
                designation: ruleArray[0],
                limiter: ruleArray[1],
              });
            } else if (ruleArray && ruleArray.length > 0) {
              this.rules.push({
                designation: ruleArray[0],
              });
            }
          }
        }
      }
    }
  }
}

export default LaravelValidationObject;
