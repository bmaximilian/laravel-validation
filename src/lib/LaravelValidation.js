import LaravelValidationObject from './LaravelValidationObject';

/**
 * Created on 21.07.17.
 *
 * @author Maximilian Beck <maximilian.beck@webteam-leipzig.de>
 */
class LaravelValidation {
  /**
   * Constructor of the laravel validation class
   * @param string
   * @param token
   * @param existingErrors
   * @param isRequired
   * @param isSemantic
   * @param messages
   */
  constructor(string,
    token,
    existingErrors = [],
    isRequired = null,
    isSemantic = false,
    messages = null,
  ) {
    this.toValidate = string;
    this.formattedValue = null;
    this.validationObject = new LaravelValidationObject(token, isRequired);
    this.errors = existingErrors;
    this.isValid = true;
    this.isSemantic = isSemantic;

    this.INTEGER_NOT_VALID_MESSAGE = messages && messages.INTEGER_NOT_VALID_MESSAGE ? messages.INTEGER_NOT_VALID_MESSAGE : 'In diesem Feld soll ein ganzzahliger Wert eingetragen werden.';
    this.NUMERIC_NOT_VALID_MESSAGE = messages && messages.NUMERIC_NOT_VALID_MESSAGE ? messages.NUMERIC_NOT_VALID_MESSAGE : 'In diesem Feld soll ein numerischer Wert eingetragen werden.';
    this.REGEX_NOT_VALID_MESSAGE = messages && messages.REGEX_NOT_VALID_MESSAGE ? messages.REGEX_NOT_VALID_MESSAGE : 'Diese Eingabe ist ungültig.';
    this.REQUIRED_MESSAGE = messages && messages.REQUIRED_MESSAGE ? messages.REQUIRED_MESSAGE : 'Dieses Feld muss ausgefüllt werden.';
    this.SEMANTIC_TOO_HIGH = messages && messages.SEMANTIC_TOO_HIGH ? messages.SEMANTIC_TOO_HIGH : 'Ein niedriger Wert ist realistischer.';
    this.SEMANTIC_TOO_LOW = messages && messages.SEMANTIC_TOO_LOW ? messages.SEMANTIC_TOO_LOW : 'Ein höherer Wert wäre realistischer.';

    this.MAX_STRING_LENGTH = messages && messages.MAX_STRING_LENGTH ? messages.MAX_STRING_LENGTH : 'Die Eingabe darf maximal {limiter} Zeichen lang sein.';
    this.MAX_NUMBER_SIZE = messages && messages.MAX_NUMBER_SIZE ? messages.MAX_NUMBER_SIZE : 'Der eingegebene Wert darf nicht größer als {limiter} sein.';
    this.MIN_STRING_LENGTH = messages && messages.MIN_STRING_LENGTH ? messages.MIN_STRING_LENGTH : 'Die Eingabe muss mindestens {limiter} Zeichen lang sein.';
    this.MIN_NUMBER_SIZE = messages && messages.MIN_NUMBER_SIZE ? messages.MIN_NUMBER_SIZE : 'Der eingegebene Wert darf nicht kleiner als {limiter} sein.';
  }

  /**
   * Main function of the class
   * @return {boolean}
   */
  validate() {
    this.validateDataType();
    this.validateRules();
    this.validateRequired();
    if (!this.formattedValue || this.formattedValue === null || isNaN(this.formattedValue)) {
      this.formattedValue = this.toValidate;
    }
    return this.isValid;
  }

  /**
   * Validates the data type of the parameter which should be updated
   */
  validateDataType() {
    switch (this.validationObject.type) {
      case 'integer':
        if (this.toValidate) {
          if (/^\d+$/.test(this.toValidate)) {
            this.formattedValue = parseInt(this.toValidate, 0);
          } else {
            this.errors.push(this.INTEGER_NOT_VALID_MESSAGE);
            this.isValid = false;
          }
        }
        break;
      case 'numeric':
        if (this.toValidate) {
          if (/^\d*([.,])?\d+$/.test(this.toValidate)) {
            this.formattedValue = parseFloat(this.toValidate.toString().replace(',', '.'));
          } else {
            this.errors.push(this.NUMERIC_NOT_VALID_MESSAGE);
            this.isValid = false;
          }
        }
        break;
      default:
        break;
    }
  }

  /**
   * Checks if the parameter is required
   */
  validateRequired() {
    if (this.validationObject.required
        && (this.toValidate !== null && this.toValidate.toString().length === 0)) {
      this.errors.push(this.REQUIRED_MESSAGE);
      this.isValid = false;
    } else if (!this.validationObject.required
      && (this.toValidate === null || this.toValidate.toString().length === 0)) {
      this.isValid = true;
      if (this.isSemantic) {
        this.errors = [];
      }
    }
  }

  /**
   * Checks the laravel vallidation rules
   */
  validateRules() {
    if (this.validationObject.rules.length > 0) {
      for (let i = 0; i < this.validationObject.rules.length; i += 1) {
        switch (this.validationObject.rules[i].designation) {
          case 'max':
            try {
              if (this.validationObject.type === 'string') {
                if (this.toValidate.length > this.validationObject.rules[i].limiter) {
                  this.errors.push(LaravelValidation.replaceStringTemplate(
                    this.MAX_STRING_LENGTH,
                    {
                      limiter: this.validationObject.rules[i].limiter,
                    }));
                  this.isValid = false;
                }
              } else if (this.validationObject.type === 'integer' || this.validationObject.type === 'numeric') {
                if (this.formattedValue > this.validationObject.rules[i].limiter) {
                  if (this.isSemantic) {
                    this.errors.push(this.SEMANTIC_TOO_HIGH);
                  } else {
                    this.errors.push(LaravelValidation.replaceStringTemplate(
                      this.MAX_NUMBER_SIZE,
                      {
                        limiter: this.validationObject.rules[i].limiter,
                      }));
                  }
                  this.isValid = false;
                }
              }
            } catch (e) {
              this.isValid = false;
            }
            break;
          case 'min':
            try {
              if (this.validationObject.type === 'integer' || this.validationObject.type === 'numeric') {
                if (this.formattedValue < this.validationObject.rules[i].limiter) {
                  if (this.isSemantic) {
                    this.errors.push(this.SEMANTIC_TOO_LOW);
                  } else {
                    this.errors.push(LaravelValidation.replaceStringTemplate(
                      this.MIN_NUMBER_SIZE,
                      {
                        limiter: this.validationObject.rules[i].limiter,
                      }));
                  }
                  this.isValid = false;
                }
              } else if (this.toValidate && this.toValidate !== null) {
                if (this.toValidate.length < this.validationObject.rules[i].limiter) {
                  this.errors.push(LaravelValidation.replaceStringTemplate(
                    this.MIN_STRING_LENGTH,
                    {
                      limiter: this.validationObject.rules[i].limiter,
                    }));
                  this.isValid = false;
                }
              }
            } catch (e) {
              this.isValid = false;
            }
            break;
          case 'regex':
            this.validationObject.rules[i].limiter = LaravelValidation.reformatRegexLimiter(
              this.validationObject.rules[i].limiter);
            if (
              !new RegExp(this.validationObject.rules[i].limiter).test(this.toValidate.toString())
            ) {
              this.errors.push(this.REGEX_NOT_VALID_MESSAGE);
              this.isValid = false;
            }
            break;
          default:
            break;
        }
      }
    }
  }

  /**
   * reformats the validation regex string from laravel-php regex to js regex
   * @param limiter
   * @return {*}
   */
  static reformatRegexLimiter(limiter) {
    if (limiter[0] === '/'
    || limiter[limiter.length - 1] === '/'
    || limiter[limiter.length - 2] === '/') {
      const limiterArray = limiter.split('/');
      let newLimiter = '';
      for (let k = 1; k < limiterArray.length - 1; k += 1) {
        newLimiter += limiterArray[k];
        if (k < limiterArray.length - 2) {
          newLimiter += '/';
        }
      }
      return newLimiter;
    }
    return limiter;
  }

  /**
   * Replaces ttemplate markers in string with values in values
   * @param template
   * @param values
   */
  static replaceStringTemplate(template, values) {
    let formatted = template;
    Object.keys(values).forEach((value) => {
      formatted = formatted.toString().replace(`<|${value}|>`, values[value].toString());
    });
    return formatted;
  }
}

export default LaravelValidation;
