module.exports = {
	root: true,
	parser: 'babel-eslint',
	parserOptions: {
		sourceType: 'module'
	},
	env: {
		browser: true,
	},
	'settings': {
    	'import/resolver': {
      		'webpack': {
        		'config': 'conf/webpack.conf.js'
      		}
    	}
  	},
    extends: 'airbnb-base',
	rules: {
	// don't require .vue extension when importing
		'import/extensions': ['error', 'always', {
			'js': 'never',
		}],
		// allow debugger during development
		'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
		// allow console during development
		'no-console': process.env.NODE_ENV === 'production' ? 2 : 0,
		// allow empty lines
		'no-trailing-spaces': ['error', {
			'skipBlankLines': true,
		}],
        quotes: [
            'error',
            'single',
        ],
        semi: [
            'error',
            'always',
        ],
	}
};
