const path = require('path');
const webpack = require('webpack');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');

function resolve (dir) {
  return path.join(__dirname, '..', dir);
}

module.exports = {

    entry: {
        script: path.resolve(__dirname, '../src/index.js')
    },

    module: {
        rules: [
            {
                test: /\.(js|vue)$/,
                loader: 'eslint-loader',
                enforce: 'pre',
                include: [resolve('src'), resolve('test')],
                options: {
                    formatter: require('eslint-friendly-formatter')
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: [resolve('src'), resolve('test')]
            },
        ]
    },

    output: {
        path: resolve('build'),
        filename: '[name].js',
        pathinfo: true
    },

    resolve: {
        extensions: ['.js', '.json'],
        alias: {
          '@': resolve('src')
        },
        modules: [
            __dirname,
            resolve('node_modules')
        ]
    },

    resolveLoader: {
	   moduleExtensions: ['-loader']
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new FriendlyErrorsPlugin()
    ]

};
