module.exports = function (config) {
    config.set({
        // basePath: '',
        frameworks: ['mocha', 'chai', 'sinon', 'phantomjs-shim'],
        files: [
            '../test/spec/**/*.spec.js'
        ],
        exclude: [],
        preprocessors: {
            '../src/**/*.js': ['coverage'],
            '../test/spec/**/*.spec.js': ['webpack', 'sourcemap']
        },
        // webpack configuration
        webpack: require('./webpack.conf.js'),
        webpackMiddleware: {
            stats: 'errors-only',
            noInfo: true,
        },
        specReporter: {
          showSpecTiming: true
        },
        reportSlowerThan: 25,
        reporters: ['spec', 'coverage'],
        // port: 9876,
        // colors: true,
        // logLevel: config.LOG_INFO,
        // autoWatch: true,
        browsers: ['PhantomJS'],
        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        // singleRun: false,
        // concurrency: Infinity,
        coverageReporter: {
          dir: '../test/coverage',
          reporters: [
            { type: 'lcov', subdir: '.' },
            { type: 'text-summary' },
          ],
        },
    });
};