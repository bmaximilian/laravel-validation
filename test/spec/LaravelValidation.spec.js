/**
 * Created on 24.09.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import LaravelValidation from '@/lib/LaravelValidation';

describe('LaravelValidation', () => {
  it('Should return true when validating a valid string', () => {
    const validation = new LaravelValidation('123', 'integer|min:100|max:200|regex:/^[0-9]+$/');
    validation.validate();
    expect(validation.isValid).to.equal(true);
    expect(validation.toValidate).to.equal('123');
    expect(validation.formattedValue).to.equal(123);
  });
  it('Should validate integer data types', () => {
    let validation = new LaravelValidation('123', 'integer');
    validation.validate();
    expect(validation.isValid).to.equal(true);
    expect(validation.toValidate).to.equal('123');
    expect(validation.formattedValue).to.equal(123);

    validation = new LaravelValidation('12ab', 'integer');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('12ab');
    expect(validation.formattedValue).to.equal('12ab');

    validation = new LaravelValidation('123', 'integer|max:100');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('123');
    expect(validation.formattedValue).to.equal(123);

    validation = new LaravelValidation('123', 'integer|min:125');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('123');
    expect(validation.formattedValue).to.equal(123);

    validation = new LaravelValidation('123', 'integer|regex:/^9+$/');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('123');
    expect(validation.formattedValue).to.equal(123);

    validation = new LaravelValidation('123', 'integer|regex:^9+$');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('123');
    expect(validation.formattedValue).to.equal(123);

    validation = new LaravelValidation('123', 'integer|regex:^[0-9]+$');
    validation.validate();
    expect(validation.isValid).to.equal(true);
    expect(validation.toValidate).to.equal('123');
    expect(validation.formattedValue).to.equal(123);
  });
  it('Should validate numeric data types', () => {
    let validation = new LaravelValidation('123.456', 'numeric');
    validation.validate();
    expect(validation.isValid).to.equal(true);
    expect(validation.toValidate).to.equal('123.456');
    expect(validation.formattedValue).to.equal(123.456);

    validation = new LaravelValidation('123,456', 'numeric');
    validation.validate();
    expect(validation.isValid).to.equal(true);
    expect(validation.toValidate).to.equal('123,456');
    expect(validation.formattedValue).to.equal(123.456);

    validation = new LaravelValidation('12.ab', 'numeric');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('12.ab');
    expect(validation.formattedValue).to.equal('12.ab');

    validation = new LaravelValidation('100.4', 'numeric|max:100.3');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('100.4');
    expect(validation.formattedValue).to.equal(100.4);

    validation = new LaravelValidation('125.5', 'numeric|min:125.6');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('125.5');
    expect(validation.formattedValue).to.equal(125.5);

    validation = new LaravelValidation('123.4', 'numeric|regex:/^9+$/');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('123.4');
    expect(validation.formattedValue).to.equal(123.4);

    validation = new LaravelValidation('123,2', 'numeric|regex:^9+$');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('123,2');
    expect(validation.formattedValue).to.equal(123.2);

    validation = new LaravelValidation('123.456', 'numeric|regex:^[0-9]+$');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('123.456');
    expect(validation.formattedValue).to.equal(123.456);
  });
  it('Should validate a string', () => {
    const validation = new LaravelValidation('teststring', 'string|regex:^test.?string$');
    validation.validate();
    expect(validation.isValid).to.equal(true);
    expect(validation.toValidate).to.equal('teststring');
    expect(validation.formattedValue).to.equal('teststring');
  });
  it('Should check required', () => {
    let validation = new LaravelValidation('teststring', 'string|required');
    validation.validate();
    expect(validation.isValid).to.equal(true);
    expect(validation.toValidate).to.equal('teststring');
    expect(validation.formattedValue).to.equal('teststring');

    validation = new LaravelValidation('1', 'integer|required');
    validation.validate();
    expect(validation.isValid).to.equal(true);
    expect(validation.toValidate).to.equal('1');
    expect(validation.formattedValue).to.equal(1);

    validation = new LaravelValidation('', 'integer|required');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('');
    expect(validation.formattedValue).to.equal('');

    validation = new LaravelValidation('', 'string|required');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('');
    expect(validation.formattedValue).to.equal('');

    validation = new LaravelValidation('1', 'required');
    validation.validate();
    expect(validation.isValid).to.equal(true);
    expect(validation.toValidate).to.equal('1');
    expect(validation.formattedValue).to.equal('1');

    validation = new LaravelValidation('', 'required');
    validation.validate();
    expect(validation.isValid).to.equal(false);
    expect(validation.toValidate).to.equal('');
    expect(validation.formattedValue).to.equal('');
  });
  it('Should replace template strings', () => {
    const template = 'Das ist ein <|type|> template';
    const formatted = LaravelValidation.replaceStringTemplate(template, { type: 'schönes' });
    expect(formatted).to.equal('Das ist ein schönes template');

    const template2 = 'Das ist <|number|> <|type|> template';
    const formatted2 = LaravelValidation.replaceStringTemplate(template2, { type: 'schönes', number: 'ein' });
    expect(formatted2).to.equal('Das ist ein schönes template');
  });
});
