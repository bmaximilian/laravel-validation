/**
 * Created on 24.09.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import ValidationToken from '@/lib/ValidationToken';

describe('ValidationToken', () => {
  const t0 = new ValidationToken(() => true);
  const t1 = new ValidationToken(() => true, true);
  const t2 = new ValidationToken(() => true, true, 1, 1);
  const t3 = new ValidationToken(() => true, true, 3, 4);
  const t4 = new ValidationToken(() => true, true, 2);
  const t5 = new ValidationToken(() => true, false, 0, 2);

  it('Can be created', () => {
    const validationToken = new ValidationToken(() => true);
    expect(validationToken instanceof ValidationToken).to.equal(true);
  });

  it('Should validate a normal token', () => {
    expect(t0.isValidToken('')).to.equal(false);
    expect(t0.isValidToken('boolean')).to.equal(true);
    expect(t0.isValidToken('boolean:')).to.equal(false);
    expect(t0.isValidToken('min:30')).to.equal(true);
    expect(t0.isValidToken('between:30,40')).to.equal(true);
    expect(t0.isValidToken('between', true)).to.equal(true);
    expect(t0.isValidToken('between:3', true)).to.equal(false);
    expect(t0.isValidToken('between:3,4', true)).to.equal(false);
  });

  it('Should validate a token with one required values', () => {
    expect(t1.isValidToken('')).to.equal(false);
    expect(t1.isValidToken('boolean')).to.equal(false);
    expect(t1.isValidToken('boolean:')).to.equal(false);
    expect(t1.isValidToken('min:30')).to.equal(true);
    expect(t1.isValidToken('between:30,40')).to.equal(false);
    expect(t1.isValidToken('between', true)).to.equal(false);
    expect(t1.isValidToken('between:3', true)).to.equal(true);
    expect(t1.isValidToken('between:3,4', true)).to.equal(false);
  });

  it('Should validate a token with required values', () => {
    expect(t2.isValidToken('')).to.equal(false);
    expect(t2.isValidToken('boolean')).to.equal(false);
    expect(t2.isValidToken('boolean:')).to.equal(false);
    expect(t2.isValidToken('min:30')).to.equal(true);
    expect(t2.isValidToken('between:30,40')).to.equal(true);
    expect(t2.isValidToken('between', true)).to.equal(false);
    expect(t2.isValidToken('between:3', true)).to.equal(true);
    expect(t2.isValidToken('between:3,4', true)).to.equal(true);
  });


  it('Should validate a token with required values', () => {
    expect(t3.isValidToken('')).to.equal(false);
    expect(t3.isValidToken('boolean')).to.equal(false);
    expect(t3.isValidToken('boolean:')).to.equal(false);
    expect(t3.isValidToken('min:30')).to.equal(false);
    expect(t3.isValidToken('between:30,40')).to.equal(false);
    expect(t3.isValidToken('between', true)).to.equal(false);
    expect(t3.isValidToken('between:3', true)).to.equal(false);
    expect(t3.isValidToken('between:3,4', true)).to.equal(false);
    expect(t3.isValidToken('between:3,4,4')).to.equal(true);
    expect(t3.isValidToken('between:3,4,4,5')).to.equal(true);
    expect(t3.isValidToken('between:3,4,4,5,6')).to.equal(false);
  });

  it('Should validate a token with required values', () => {
    expect(t4.isValidToken('')).to.equal(false);
    expect(t4.isValidToken('boolean')).to.equal(false);
    expect(t4.isValidToken('boolean:')).to.equal(false);
    expect(t4.isValidToken('min:40')).to.equal(false);
    expect(t4.isValidToken('between:40,40')).to.equal(true);
    expect(t4.isValidToken('between', true)).to.equal(false);
    expect(t4.isValidToken('between:4', true)).to.equal(false);
    expect(t4.isValidToken('between:4,4', true)).to.equal(true);
    expect(t4.isValidToken('between:4,4,4')).to.equal(true);
    expect(t4.isValidToken('between:4,4,4,5')).to.equal(true);
    expect(t4.isValidToken('between:3,4,4,5,6')).to.equal(true);
  });

  it('Should validate a token with required values', () => {
    expect(t5.isValidToken('')).to.equal(false);
    expect(t5.isValidToken('boolean')).to.equal(true);
    expect(t5.isValidToken('boolean:')).to.equal(false);
    expect(t5.isValidToken('min:40')).to.equal(true);
    expect(t5.isValidToken('between:40,40')).to.equal(true);
    expect(t5.isValidToken('between', true)).to.equal(true);
    expect(t5.isValidToken('between:4', true)).to.equal(true);
    expect(t5.isValidToken('between:4,4', true)).to.equal(true);
    expect(t5.isValidToken('between:4,4,4')).to.equal(false);
    expect(t5.isValidToken('between:4,4,4,5')).to.equal(false);
    expect(t5.isValidToken('between:3,4,4,5,6')).to.equal(false);
  });

  it('Should execute the function', () => {
    const executeFunction = (object, token) => {
      try {
        return object.function(null, token);
      } catch (e) {
        return false;
      }
    };
    const token1 = new ValidationToken(value => value === null);

    expect(executeFunction(token1, 'bool')).to.equal(true);
    expect(executeFunction(token1, '')).to.equal(false);
  });
});
